Public Domain Java Software DB
==============================
This is a database/repo of public domain java software. Each project is stored as it's original eclipse project directory. Read the README.md inside the project dir for info on that project.
## Submitting an entry
Entries can be submitted by emailing publicdomain@htmlguy.cu.cc. Please package your eclipse project in a ZIP file, including UNLICENSE/LICENSE and a README.md, and attatch it to the email. After that, send further .ZIP files including updates. NOTE: DO NOT UPDATE THE ACTUAL SOURCE CODE, ONLY UPDATE DOCUMENTATION! ANY CHANGES IN /SRC, EXCEPT **IMPORTANT** BUG FIXES, WILL BE IGNORED.