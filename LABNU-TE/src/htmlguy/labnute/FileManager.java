/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
 */
package htmlguy.labnute;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FileManager {
	public static String save(String contents, String file, Editor e, boolean as) {
		File f = null;
		if (file == null || as) {
			JFileChooser fc = new JFileChooser();
			if(file != null) {
				fc.setSelectedFile(new File(file));
			}
			int returnVal;
			if(as) {
				returnVal = fc.showDialog(e, "Save As");
			} else {
				returnVal = fc.showSaveDialog(e);
			}
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				f = fc.getSelectedFile();
				file = f.getPath();
			} else {
				return null;
			}
		} else {
			f = new File(file);
		}
		try {
			BufferedWriter buff = new BufferedWriter(new FileWriter(f));
			buff.write(contents);
			buff.close();
			return file;
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(e, "Unable to save to this file!",
					"Saving Error", JOptionPane.WARNING_MESSAGE);
			e1.printStackTrace();
			return null;
		}
	}

	public static FileOpened open(Editor e) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showOpenDialog(e);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			try {
				BufferedReader buff = new BufferedReader(new FileReader(file));
				String outfile = "", line;
				while ((line = buff.readLine()) != null) {
					outfile += line + "\n";
				}
				buff.close();
				return new FileOpened(file.getPath(), outfile);
			} catch (IOException e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(e, "Unable to open this file!",
						"Opening Error", JOptionPane.WARNING_MESSAGE);
				return null;
			}
		}
		return null;
	}

	public static FileOpened open(Editor e, String name) {
		File file = new File(name);
		try {
			BufferedReader buff = new BufferedReader(new FileReader(file));
			String outfile = "", line;
			while ((line = buff.readLine()) != null) {
				outfile += line + "\n";
			}
			buff.close();
			return new FileOpened(file.getPath(), outfile);
		} catch (IOException e1) {
			e1.printStackTrace();
			JOptionPane.showMessageDialog(e, "Unable to open this file!",
					"Opening Error", JOptionPane.WARNING_MESSAGE);
			return null;
		}
	}
}
