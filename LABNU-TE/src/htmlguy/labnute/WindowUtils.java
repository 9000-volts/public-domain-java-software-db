/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
 */
package htmlguy.labnute;
import java.awt.Dimension;
import java.awt.Toolkit;


public class WindowUtils {
	public static Dimension seggestSize() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension seggest = new Dimension();
		int closestSize = getClosestDimensions(dim.width, new int[] {1024, 1280, 1366});
		switch(closestSize) {
		case 1280: // 1280x800 - THIRD MOST COMMON SIZE
			seggest.width = 800;
			seggest.height = 750;
			break;
		case 1024: // 1024x768 - SECOND MOST COMMON SIZE
			seggest.width = 500;
			seggest.height = 700;
			break;
		case 1366: // 1366x768 - MOST COMMON SIZE
		default:
			seggest.width = 1000;
			seggest.height = 700;
		}
		return seggest;
	}
	private static int getClosestDimensions(int of, int[] in) {
	    int min = Integer.MAX_VALUE;
	    int closest = of;

	    for (int v : in) {
	        final int diff = Math.abs(v - of);

	        if (diff < min) {
	            min = diff;
	            closest = v;
	        }
	    }

	    return closest;
	}
}
