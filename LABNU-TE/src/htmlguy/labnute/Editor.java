/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
 */
package htmlguy.labnute;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

public class Editor extends JFrame implements KeyListener {
	private static final long serialVersionUID = 1L;
	String prevfile = null;
	String fname = null;
	JTextArea a = new JTextArea();

	public Editor(String filename, boolean isnu) {
		super(filename + " - LABNU-TE");
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch(Exception e) {
			try {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GtkLookAndFeel");
			} catch (Exception er) {
			}
		}
		fname = filename;
		if (isnu) {
			prevfile = filename;
			FileOpened fo = FileManager.open(this, filename);
			if (fo != null) {
				prevfile = fo.name;
				a.setText(fo.text);
			}
		}
		setSize(WindowUtils.seggestSize());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addKeyListener(this);
		a.addKeyListener(this);
		a.setTabSize(4);
		add(new JScrollPane(a));
		setVisible(true);
	}

	public static void main(String[] args) {
		if (args.length > 0) {
			new Editor(args[0], true);
		} else {
			new Editor("New File", false);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (!(e.isControlDown() || e.isAltDown() || (e.getKeyCode() != KeyEvent.VK_S && e
				.getKeyCode() != KeyEvent.VK_O) && e.isControlDown()))
			setTitle("*" + fname + " - LABNU-TE");
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_F1) {
			new WebHelp();
		}
		if (e.getKeyCode() == KeyEvent.VK_F2) {
			save();
		}
		if (e.getKeyCode() == KeyEvent.VK_F3) {
			open();
		}
		if (e.getKeyCode() == KeyEvent.VK_F4) {
			saveAs();
		}
		if(e.getKeyCode() == KeyEvent.VK_F5) {
			new PublicDomain();
		}
		if (e.isControlDown()) {
			if (e.getKeyCode() == KeyEvent.VK_S) {
				if (e.isShiftDown()) {
					saveAs();
				} else {
					save();
				}
			}
			if (e.getKeyCode() == KeyEvent.VK_O) {
				open();
			}
			if(e.getKeyCode() == KeyEvent.VK_L) {
				new PublicDomain();
			}
		}
	}
	public void save() {
		String exp = FileManager.save(a.getText(), prevfile, this, false);
		if (exp != null) {
			prevfile = fname = exp;
			setTitle(fname + " - LABNU-TE");
		}
	}
	public void saveAs() {
		String exp = FileManager.save(a.getText(), prevfile, this, true);
		if (exp != null) {
			prevfile = fname = exp;
			setTitle(fname + " - LABNU-TE");
		}
	}
	public void open() {
		FileOpened fo = FileManager.open(this);
		if (fo != null) {
			prevfile = fname = fo.name;
			a.setText(fo.text);
			setTitle(fname + " - LABNU-TE");
		}
	}
}
