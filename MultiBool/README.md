MultiBool
=========
MultiBool is a simple API for storing multiple boolean values inside of an int.
It can be used to pass a varying amount of boolean values to a method with
only one argument for all of these values. Documentation NOT YET AVAILABLE (for now, please generate a javadoc or inspect the source)!
