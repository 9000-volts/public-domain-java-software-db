/*
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>
*/
package htmlguy.multibool;
/**
 * A simple library for the use of MultiBool-ints (ints that are actually multiple boolean values).
 * @author htmlguy
 * @since 1.0
 * @version 1.0
 */
public class MultiBool {
	/**
	 * Stores options for multibool behavior.
	 * @since 1.0
	 */
	private int operations = 0;
	/**
	 * Initializes MultiBool with VERBOSE and DEBUG options. This constructor takes one multibool-int containing the options for VERBOSE output and DEBUGging.
	 * @param operations a multibool-int containing the 'VERBOSE' and 'DEBUG' options.
	 * @since 1.0
	 */
	public MultiBool(int operations) {
		this.operations = operations;
		if((operations & VERBOSE) == VERBOSE) {
			System.out.println("  MultiBool Instance Initialized [VERBOSE]" + ((operations & DEBUG) == DEBUG ? "[DEBUG]" : ""));
		}
		if((operations & DEBUG) == DEBUG) {
			System.out.println("  Initialized with ops: " + operations);
		}
	}
	/**
	 * Initializes MultiBool with no verbose and no debugging.
	 * @since 1.0
	 */
	public MultiBool() {}
	/**
	 * Gets the value of a flag in a multibool-int.
	 * @param multibool the multibool-int to get the value of a flag in.
	 * @param whatbool the flag to get the value of.
	 * @return the value of the flag in the multibool-int.
	 * @since 1.0
	 */
	public boolean check(int multibool, int whatbool) {
		boolean res = ((multibool & whatbool) == whatbool);
		if((operations & VERBOSE) == VERBOSE) {
			System.out.println("    MultiBool testing/checking.");
		}
		if((operations & DEBUG) == DEBUG) {
			System.out.println("    multibool: " + multibool + "  whatbool: " + whatbool + "  RETURNING: " + res);
		}
		return res;
	}
	/**
	 * Checks whether or not a multibool-int has all flags false.
	 * @param multibool the multibool-int to check whether or not all flags are false.
	 * @return whether or not all flags in the multibool-int are false.
	 * @since 1.0
	 */
	public boolean none(int multibool) {
		boolean res = (multibool == 0);
		if((operations & VERBOSE) == VERBOSE) {
			System.out.println("    MultiBool testing FOR NONE.");
		}
		if((operations & DEBUG) == DEBUG) {
			System.out.println("    multibool: " + multibool + "  RETURNING: " + res);
		}
		return res;
	}
	/**
	 * Returns the best working value for the next final flag to be set.
	 * @param previous the assigned value of the previous value to be set, or 0 if none.
	 * @return the assigned next working value.
	 * @since 1.0
	 */
	public int assignNext(int previous) {
		int res = previous * 2;
		if(previous == 0) {
			res = 1;
		}
		if((operations & VERBOSE) == VERBOSE) {
			System.out.println("    MultiBool assigning NEXT VALUE.");
		}
		if((operations & DEBUG) == DEBUG) {
			System.out.println("    previous: " + previous + "  RETURNING: " + res + "  FIRST: " + (previous == 0));
		}
		return res;
	}
	/**
	 * VERBOSE flag - enable verbose output. Can be passed when MultiBool is initialized, and combined with the DEBUG flag, like new MultiBool(VERBOSE | DEBUG).
	 * @since 1.0
	 */
	public static final int VERBOSE = 0x01;
	/**
	 * DEBUG flag - enable debugging output. Can be passed when MultiBool is initialized, and combined with the VERBOSE flag, like new MultiBool(VERBOSE | DEBUG).
	 * @since 1.0
	 */
	public static final int DEBUG = 0x02;
}
