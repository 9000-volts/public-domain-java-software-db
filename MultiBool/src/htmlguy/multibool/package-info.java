/**
 * Contains all multibool library code.
 * @author htmlguy
 * @version 1.0
 * @since 1.0
 */
package htmlguy.multibool;